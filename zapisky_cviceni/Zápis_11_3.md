Požadavky na součinnost
-        konzultace správnosti
-        podporu do uvedení do provozu
Business cíle
-        měřitelnost efektivity komunikace
-        způsoby komunikace, kdy se kdo setkává
-        rychlost procesu se dá měřit dlouhodobě
-        snížení chybovosti dodaného zboží při tvorbě objednávky
Zákazník
-        je to uživatel
-        kurýr
Stakeholder
-        nemusí být nutně člověk který má nějaké požadavky
Výstupy
-        testovací scénáře
Rizika
-        chybí mitigace
-        chyba v aplikaci tam nemá být
-        dopad chybí
-        specifikovat problémy s tabletem, nebo to neřešit
-        špatný dosah mobilního internetu
                                                         
Business model
-        kurýr má trasu, to je entita, pracovní oblast
-        entita skladu (malý a velký), regál jako entitu možná
-        přesun mezi sklady vyjádřit
 
Procesy
-        nemá představu co kurýr dělá
-        procesy mají popsat, co se děje s objednávkou
-        stavy a pravidelnosti vyjádřit
-        gateway, která může vytvořit plán trasy
-        okomentovat, že kurýr píše prodané zboží skladníkovi
-        dělat to jak je to teď a ne jak to bude s tou aplikací
