### UC 01 - Přihlášení uživatele do systému
1.	Systém zobrazí přihlašovací okno.
2.	Uživatel vyplní údaje a vybere svou pozici.
3.	IF Systém ověří korektnost údajů THEN zobrazí hlavní stránku

### UC 02 – Detail zboží
1.	Systém zobrazí seznam zboží.
2.	Uživatel klikne na obrázek požadovaného produktu.
3.	Systém zobrazí detail produktu.

### UC 03 – Odhlášení
1.	Systém zobrazuje tlačítko „odhlásit“.
2.	Uživatel klikne na tlačítko „odhlásit“.

### UC 04 – Zobrazení stavu skladu.
1.	Systém zobrazí stav skladu.
 
### UC 05 – Zobrazení prodaného zboží
1.	INCLUDE (zobrazení stavu skladu).
2.	Uživatel klikne na tlačítko „prodané zboží“.
3.	Systém zobrazí prodané zboží.
 
### UC 06 – Aktualizace prodaného zboží
1.	INCLUDE (zobrazení prodaného zboží).
2.	Systém zobrazí tlačítka na přidání produktu a potvrzení doplnění zboží.
3.	Uživatel přidá doplněné produkty a potvrdí doplnění.
 
### UC 07 – Zobrazení detailu objednávky
1.	INCLUDE (zobrazení seznamu objednávek).
2.	Uživatel klikne na řádek s objednávkou.
3.	Systém zobrazí detail objednávky.
 
### UC 08 – Zobrazení seznamu objednávek
1.	Systém zobrazí na hlavní stránce tlačítko „seznam objednávek“.
2.	Uživatel klikne na tlačítko „seznam objednávek“.
3.	Systém zobrazí seznam objednávek.
 
### UC 09 – Zobrazení souhrnu plateb za směnu
1.	Systém zobrazí na hlavní stránce tlačítko „souhrn plateb“.
2.	Uživatel klikne na tlačítko „souhrn plateb.
3.	Systém zobrazí souhrn plateb.

### UC 10 – Zobrazení hodnocení objednávek
1.	Systém zobrazí na hlavní stránce tlačítko „hodnocení objednávek“.
2.	Uživatel klikne na tlačítko „hodnocení objednávek“.
3.	Systém zobrazí hodnocení objednávek.

### UC 11 – Potvrzení objednávky
1.	INCLUDE (detail objednávky).
2.	Uživatel klikne na potvrdit\odmítnout.


### UC 12 – Evidence stavu doručení
1.	INCLUDE (detail objednávky).
2.	Uživatel klikne na doručeno\zrušeno.

### UC 13 – Zobrazení statistiky prodejů 
1.	Systém zobrazí na hlavní stránce tlačítko „statistiky“.
2.	Uživatel na tlačítko klikne.
3.	Systém zobrazí statistiky.

### UC 14 – Zobrazení přihlášení zaměstnanců
1.	Systém zobrazí na hlavní stránce tlačítko „docházka zaměstnanců“.
2.	Uživatel na tlačítko klikne.
3.	Systém zobrazí docházku.