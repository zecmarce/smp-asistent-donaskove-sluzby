* Zákazník chce mít přehled o nabízeném zboží, aby věděl co si objednat.
* Zákazník chce znát cenu zboží, aby věděl zda to vyhovuje jeho finančním možnostem.
* Zákazník chce znát způsoby platby, aby vědel jestli může platit online.
* Zákazník chce znát plánovaný čas doručení, aby věděl zda to vyhovuje jeho časovým možnostem. 
* Zákazník chce vědět stanovený čas doručení, aby věděl, kdy se má nacházet v místě doručení.
* Zákazník chce možnost hodnotit rychlost doručení objednávky, aby inicioval zkvalitnení služby.
* Zákazník chce možnost hodnotit schování kurýra, aby inicioval zkvalitnení služby.
* Skladník potřebuje znát číslo regálu naskladňovaného zboží, aby věděl kam zboží naskladnit.
* Skladník chce znát stav chybejíciho zboží malého skladu, aby věděl, jestli má něco doplnit.
* Skladník potřebuje evidovat svoji směnu, aby dostal zaplaceno za svou práci. 
* Kurýr chce mít možnost zadávání objednávky, aby mohl spracovat telefonickou objednávku. 
* Kurýr chce znát adresu doručení, aby věděl kam má jet.
* Kurýr chce znát obsah objednávky, aby ji mohl expedovat.
* Kurýr chce znát stav malého skladu, aby věděl, zda je možné telefonickou objednávku uskutečnit
* Kurýr chce vědět počet objednávek v danou chvíli, aby mohl zvolit plán trasy.
* Kurýr chce mít přehled o množství objednaného zboží, aby věděl na kolik výjezdů je možné objednávky uskutečnit.
* Kurýr chce znát způsob platby, aby věděl jestli má očekávat platbu v hotovosti.
* Kurýr chce znát telefonní číslo zákazníka, aby ho mohl kontaktovat při nemožnosti doručení objednávky.
* Kurýr chce znát telefonní číslo zákazníka, aby ho mohl kontaktovat při změně času doručení.
* Kurýr chce znát telefonní číslo zákazníka, aby ho mohl kontaktovat při nepřítomnosti zákazníka.
* Kurýr potřebuje znát číslo regálu objednaného zboží, aby věděl odkud zboží vezme.
* Kurýr potřebuje vědět finanční obrat za směnu, aby mohl přepočítat kolik peněz má odevzdat.
* Kurýr potřebuje evidovat svoji směnu, aby dostal zaplaceno za svou práci. 
* Majitel chce vidět přehled prodaného zboží, aby viděl příjmy z prodeje.
* Majitel chce vidět statistiky prodaného zboží, aby mohl upravit sortiment. 
* Majitel chce znát názor zákazníka na ponúkaný sortiment, aby mohl sortiment doplnit o žádané zboží.
* Majitel chce evidenci zaměstnanců, aby mohl delegovat směny.  
* Majitel chce mít přehled o směnách, kvůli delegaci zodpovědnosti.
* Majitel chce vědět počet zrušených objednávek, aby dokázal zkvalitnit nabízené služby.
* Majitel chce vědět zpětnou vazbu zákazníka, aby mohl zlepšit služby.
* Majitel chce vědět počet objednávek v určité dny, aby mohl přizpůsobit garantovaný čas doručení.
* Majitel chce mít prehled o oblastech prodeje, aby mohl lépe cílit reklamu.