	1. Varianta
	• Jeden zaměstnanec na místo rozvozu vybavuje telefonické objednávky a chystá zboží a případně plánuje trasy pro kurýry.
	• Nákup notebooku a evidence prodaného zboží je v excel tabulkách.
	• Nabídka zboží zveřejněna na sociálních sítích.
	• Skladník doplňuje zboží na základě informací z excel tabulky.
    + Nízká cena
    + Jednoduchost implementace
    - Vyšší naročnost práce
    - Zastaralé řešení
	

	2. Varianta
	• Webová aplikace pro zákazníky na tvorbu objednávek.
	• Android aplikace na tabletu kurýra přebírá data z webové aplikace.
	• Veškerá evidence a správa objednávek probíhá v android aplikaci.
	• Skladník doplňuje zboží na základě informací z android aplikace.
    + Zvýšení efektivity práce
    + Urychlení prodejního procesu
    + Moderní řešení, které zaujme zákazníky
    + Vyšší průhlednost dění ve firmě
    - Vyšší cena
    - Náročnější implementace


	3. Varianta
	• Android a iOS aplikace pro kurýra a skladníka.
    • Mobilní aplikace místo webové i pro zákazníky.  
	• Aplikace obsahuje role (účty) skrze ty jsou udělovány práva
		○ Skladník, Kurýr
			§ Správa a evidence
		○ Zákazník
			§ Tvorba objednávek
    + Výrazná uživatelská přívetivost
    + Dostupnost na všech mobilních platformách
    - Výrazně vyšší cena
    - Výrazně naročnější implementace
    - Vyšší pravděpodobnost chyby v aplikaci


Zvolili jsme 2. variantu z důvodů finanční dostupnosti, ne až tak náročné implementace, 
adekvátní uživatelské přívetivosti, která odpovídá dnešním standardům.

	
	
