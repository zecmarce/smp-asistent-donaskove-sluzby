# Vykaz prace


### novy je tu ▼
[VYKAZ PRACE](https://docs.google.com/spreadsheets/d/18832comawtd1qgMXst5cU1c3Cy4skqzIGR9_-G80PIQ/edit?usp=sharing)
### novy je tu ▲
<hr>

### Sandra Hamrakova

| datum | popis | cas |
| ------ | ------ |  ------ |
|1. tyden| | |
|02.3. |analyza - zakaznik,stakeholders |30 min |
|03.3. | diskusia - zmeny |15 min |
|2. tyden| | |
|05.03.|Úprava analýzy|10 min|
|06.3.|stretnutie - BDM,BPM| 1h 30 min|
|10.03.|Oponentura|20 min|
|10.03.|BPM|45 min|
|3. tyden| | |
|13.03.|stretnutie| 2h|
|17.03.|uprava BPM| 1h|
|4. tyden| | |
|18.03.|stretnutie| 1h|
|20.03.|stretnutie| 2h|
|22.03.|oponentura|30 min|
|5. tyden| | |
|27.03.|stretnutie| 1h 45min|
|6. tyden| | |
|01.04.|stretnutie| 1h |
|03.04.|stretnutie| 3h 15min |
|07.04.|wireframes| 10 min|
|7. tyden| | |
|08.04.|wireframes| 20 min|
|10.04.|stretnutie| 2h |
|SPOLU| 1200|18h 30min|
<hr>

### Lukas Simon

| datum | popis | cas |
| ------ | ------ |  ------ |
|1. tyden|  |  |
|2.3. |Finanční analýza |40min |
|3.3 |diskuze - úpravy |15 min |
| 2. tyden |  |  |
|06.3.|stretnutie - BDM,BPM| 1h 30 min|
|09.03.|Úprava finanční analýzy|15 min|
|3. tyden| | |
|13.03.|stretnutie| 2h|
|17.03.|Úprava rizik| 10 min|
|4. tyden| | |
|18.03.|Zápis ze cvičení |15 min|
|18.03.|stretnutie| 1h|
|20.03.|stretnutie| 2h|
|22.03.|oponentura|40 min|
|5. tyden| | |
|27.03.|stretnutie| 1h 45min|
|6. tyden| | |
|01.04.|stretnutie| 1h |
|03.04.|stretnutie| 3h 15min |
|7.4.|oponentura|15 min|
|7. tyden| | |
|10.04.|stretnutie| 2h |
<hr>

### Josef Vyskocil

| datum | popis | cas |
| ------ | ------ |  ------ |
|1. tyden|  |  |
|03.03. |výstupy |30 min |
|03.03. |rizika  |10 min |
|03.03.| diskuze - upravy|5 min|
|04.03.|záznam ze cvičení|10 min|
| 2. tyden |  |  |
|06.3.|stretnutie - BDM,BPM| 1h 30 min|
|09.03.|Business model|15 min|
|10.03.|Oponentura|20 min|
|3. tyden| | |
|13.03.|stretnutie| 2h|
|4. tyden| | |
|18.03.|stretnutie| 1h|
|20.03.|stretnutie| 2h|
|24.03.|Oponentura|20 min|
|5. tyden| | |
|27.03.|stretnutie| 1h 45min|
|6. tyden| | |
|01.04.|stretnutie| 1h |
|03.04.|stretnutie| 3h 15min |
|7.4.|oponentura|15 min|
|7. tyden| | |
|10.04.|stretnutie| 2h |
<hr>

### Marcel Zec

| datum | popis | cas |
| ------ | ------ |  ------ |
|1. tyden|  |  |
|26.2. |zalozenie Git |10 min |
| 02.3.|sablona dokumentu | 10 min |
| 02.3.|analyza - uvod,popis,zadavatel | 40 min |
|03.3.|diskusia - zmeny|15 min|
|03.3.|ciele|15 min|
|03.3.|prezentacia na cviko|20 min|
| 2. tyden |  |  |
|05.3.|uprava business cielov|10 min|
|06.3.|stretnutie - BDM,BPM| 1h 30 min|
|10.3.|oponentura + dokument| 30 min|
|10.3.|prezentacia| 20 min|
|3. tyden| | |
|13.03.|stretnutie| 2h|
|17.03.|rizika|20 min|
|17.03.|prezentace|15 min|
|4. tyden| | |
|18.03.|stretnutie| 1h|
|20.03.|stretnutie| 2h|
|21.03.|uprava BPM| 40 min|
|21.03.|priprava dokumentu na oponenturu| 10 min|
|22.03|oponentura| 30 min|
|23.03|use case diagam| 1 hod|
|24.03|oponentura - dokument| 15 min|
|25.03|prezentace| 10 min|
|5. tyden| | |
|26.03.|uprava BPM | 10 min|
|5. tyden| | |
|27.03.|stretnutie| 1h 45min|
|31.03.|prezentacia| 10 min|
|6. tyden| | |
|01.04.|stretnutie| 1h |
|03.04.|stretnutie| 3h 15min |
|08.04.|prezentacia| 15 min|
|7. tyden| | |
|08.04.|wireframes| 20 min |
|10.04.|stretnutie| 2h |
|14.04.|priprava na cviko | 20 min |
|SPOLU| |21h 55min |
