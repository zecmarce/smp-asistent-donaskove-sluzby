# SMP - Asistent donáškové služby

### PREZENTACE
[ O projekte ](https://docs.google.com/presentation/d/1FNJfnNtMaQ4cnS8ikpUSVEpPoZFinRfCKnU7xlmp-MY/edit?usp=sharing) | 
[02. tyden 04.03.](https://docs.google.com/presentation/d/1c6qTEJI0AXNwUNfu3pSEVfEmE5WqeP0WUcBwA1iHHeI/edit?usp=sharing) | 
[03. tyden 11.03.](https://docs.google.com/presentation/d/1MbDruDgD3ACgLlGmbG2pjlkADhV2bGulc3FvtmhFqw8/edit?usp=sharing) | 
[04. tyden 18.03.](https://docs.google.com/presentation/d/1HUNC_o-3ygIiRswXVPtQkKvAUY-aJBuxPb9AIlmcn64/edit?usp=sharing) | 
[05. tyden 25.03.](https://docs.google.com/presentation/d/1ci505GEN1Xw1BKBygXYsPID0Or1teRqb9IKt3b78Kt0/edit?usp=sharing) | 
[06. tyden 01.04.](https://docs.google.com/presentation/d/1YZvhXMTtMg1l1J01QjilwF6EWtc4_Ik5jex4q6SHwmg/edit?usp=sharing) | 
[07. tyden 08.04.](https://docs.google.com/presentation/d/1aydvcfbDxwFscb8e0xYcg8GaJivCiIw2xBp7xKGEZkE/edit?usp=sharing) | 
[08. tyden 15.04.](https://docs.google.com/presentation/d/1eToEuZ77RVp6tHp8j_8byl_G1J0DEi8MQZYa8hkIyV4/edit?usp=sharing) |

[10. tyden 28.04.](https://docs.google.com/presentation/d/1KXJCriBLkEDLHGUAX7hhW_mMls2SE8T002lG2WDmLk4/edit?usp=sharing) |
[11. tyden 06.05.](https://docs.google.com/presentation/d/1KsHc0iV87gD1NbAeN1gDGSHUZk4KcIJ6jdIswp71yVQ/edit?usp=sharing) |
**[Finální prezentace](https://docs.google.com/presentation/d/1pd4kP-1CVMa3DU77tJFtRPeiv-6NyVk1odTnGe9MfdY/edit?usp=sharing)**

<hr>

### DOKUMENTY
**[Finální dokumentace](https://gitlab.fel.cvut.cz/zecmarce/smp-asistent-donaskove-sluzby/blob/master/Asistent_donaskove_sluzby_dokumentace.pdf)**

**[Klikatelní prototyp - firemní část](https://gitlab.fel.cvut.cz/zecmarce/smp-asistent-donaskove-sluzby/tree/master/Axure%20(Klikateln%C3%BD%20prototyp)%20/Firemn%C3%AD)**

**[Klikatelní prototyp - zákaznická část](https://gitlab.fel.cvut.cz/zecmarce/smp-asistent-donaskove-sluzby/tree/master/Axure%20(Klikateln%C3%BD%20prototyp)%20/Z%C3%A1kazn%C3%ADk)**

[ADM](https://gitlab.fel.cvut.cz/zecmarce/smp-asistent-donaskove-sluzby/blob/master/diagramy/adm.pdf)

[Model nasazení a stavový diagram](https://gitlab.fel.cvut.cz/zecmarce/smp-asistent-donaskove-sluzby/blob/master/diagramy/modelNasazeni_stavovyDiagram.pdf)

[Model komponent a sekvenční model](https://gitlab.fel.cvut.cz/zecmarce/smp-asistent-donaskove-sluzby/blob/master/diagramy/modelKomponent_sekvencniModel.pdf)

[Pracovna verzia dokumentu](https://docs.google.com/document/d/1nVAc2N6DnUkh1hGiWgo4Vtp4Ibz6rNOndauQDP3FJd0/edit?usp=sharing)

[Výkaz práce](https://docs.google.com/spreadsheets/d/18832comawtd1qgMXst5cU1c3Cy4skqzIGR9_-G80PIQ/edit?usp=sharing)
<hr>

### OPONENTURY

|TEMA|KU OPONENTURE|OPONENTURA|
|---|---|---|
|business analyza|[naša práca](https://gitlab.fel.cvut.cz/zecmarce/smp-asistent-donaskove-sluzby/blob/master/AsistentDonaskoveSluzby_analyza.pdf)|[týmu č.3](https://gitlab.fel.cvut.cz/zecmarce/smp-asistent-donaskove-sluzby/blob/master/oponentury/Oponentura_1.pdf)|
|BDM,BPM,BRQs,varianty|[naša práca](https://gitlab.fel.cvut.cz/zecmarce/smp-asistent-donaskove-sluzby/blob/master/ku_oponenture/tym1_cast2.pdf)|[týmu č.2](https://gitlab.fel.cvut.cz/zecmarce/smp-asistent-donaskove-sluzby/blob/master/oponentury/Oponentura_2.pdf)|
|SRQs,UCM,ADM|[naša práca](https://gitlab.fel.cvut.cz/zecmarce/smp-asistent-donaskove-sluzby/blob/master/ku_oponenture/tym1_cast3.pdf)|[týmu č.4](https://gitlab.fel.cvut.cz/zecmarce/smp-asistent-donaskove-sluzby/blob/master/oponentury/Oponentura_3.pdf)|
|navrh aplikace|[naša práca](https://gitlab.fel.cvut.cz/zecmarce/smp-asistent-donaskove-sluzby/blob/master/ku_oponenture/tym1_cast4.pdf)|[týmu č.2](https://gitlab.fel.cvut.cz/zecmarce/smp-asistent-donaskove-sluzby/blob/master/oponentury/Oponentura_4.pdf)|
|finalny dokument|[naša práca](https://gitlab.fel.cvut.cz/zecmarce/smp-asistent-donaskove-sluzby/blob/master/Asistent_donaskove_sluzby_dokumentace.pdf)|[týmu č.3](https://gitlab.fel.cvut.cz/zecmarce/smp-asistent-donaskove-sluzby/blob/master/oponentury/Oponentura_5.pdf)|

<hr>

### ZAPISKY ZE CVICENI
[cviceni 4.3.](https://docs.google.com/document/d/1o6q4BVdJv9o_E9Phh11zZO6q1GhHTQJCXCcyzNaRaT4/edit?usp=sharing) | 
[cviceni 11.3.](https://docs.google.com/document/d/1iG_yFXYoWWpRujEktAGg1A2tgMR0AsdmQov6prP9H4g/edit?usp=sharing) | 
[cviceni 1.4.](https://docs.google.com/document/d/1Bs-6JisMJmXKr-YqKlc-NqqqZrQV6cTQXvbJioQBe5E/edit?usp=sharing)